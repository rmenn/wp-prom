provider "digitalocean" {
  token = "${var.do-token}"
}

provider "grafana" {
  url  = "http://${digitalocean_droplet.container.ipv4_address}:3000/"
  auth = "admin:${var.grafana-password}"
}
