This is a demo setting up word press with monitoring using prometheus on a coreos machine

This does the following
* Spins up a coreos droplet in Digital Ocean blr1
* Configures via userdata to run the following containers
 * prometheus
 * prometheus node exporter
 * grafana
 * mysql
 * wordpress

Inorder for the demo to work the following needs to be done

* add the respective values for `do-token`, `mysql-password`, `grafana-password` & `ssh-key-id`
* Run `terraform apply`
* The First apply will fail as terraform does not wait for grafana to start
* Once grafana is up run `terraform apply` again
  * Adds prometheus as datasource
  * Adds Dashboard called wordpress
