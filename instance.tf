resource "digitalocean_droplet" "container" {
  name      = "container"
  size      = "2gb"
  image     = "coreos-stable"
  region    = "blr1"
  ssh_keys  = ["${var.ssh-key-id}"]
  user_data = "${data.template_file.mysql.rendered}"
  tags      = ["${digitalocean_tag.container-tag.id}"]
}

resource "digitalocean_tag" "container-tag" {
  name = "container"
}
