resource "grafana_data_source" "metrics" {
  type          = "prometheus"
  name          = "prom"
  url           = "http://localhost:9090/"
}

resource "grafana_dashboard" "metrics" {
  config_json = "${file("config/dashboard.json")}"
}
