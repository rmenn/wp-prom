data "template_file" "userdata" {
  template = "${file("config/userdata.yml")}"

  vars {
    mysql-password   = "${var.mysql-password}"
    grafana-password = "${var.grafana-password}"
  }
}
